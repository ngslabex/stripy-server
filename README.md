# STRipy's server
Running STRipy's server activates the "Local" option on the [STRipy's Client](https://gitlab.com/andreassh/stripy-client) and allows to genotype files in your computer or internal network.

## System requirements
STRipy's server is without a graphical interface can easily run on macOS, Linux or Windows 10/11 through WSL. STRipy's server requires to have [Python 3.8+](https://www.python.org/downloads/) installed on your system and Python libraries specified in the [requirements.txt](./requirements.txt) file. In addition, the server requires access to ExpansionHunter, REViewer and Samtools programs (find links and instructions below). We have measured the average RAM usage of STRipy's server v1.2 is around 80-90 MB. Minimum disk space depends mostly on the size of the reference genomes as STRipy itself is approximately 200 KB. When installing the server on another computer, then the used port (default is 5000) has to be open for incoming connections.

## Setting up server
1. Clone the repository to create a local copy on your system by running the following command in terminal:
`git clone git@gitlab.com:andreassh/stripy-server.git`

2. Set up reference FASTA files. Copy desired reference genomes into the [reference/genomes](reference/genomes) folder under the name of either _hg38.fa_ or _hg19.fa_. Unless you have a reference genome you want to use, feel free to downloaded relevant FASTA files from here:
    * hg38 (GRCh38.p12) genome: [https://hgdownload.soe.ucsc.edu/goldenPath/hg38/bigZips/latest/hg38.fa.gz](https://hgdownload.soe.ucsc.edu/goldenPath/hg38/bigZips/latest/hg38.fa.gz)
    * hg19.p13 genome: [https://hgdownload.soe.ucsc.edu/goldenPath/hg19/bigZips/p13.plusMT/hg19.p13.plusMT.fa.gz](https://hgdownload.soe.ucsc.edu/goldenPath/hg19/bigZips/p13.plusMT/hg19.p13.plusMT.fa.gz)
        
    If you are using different reference genomes than the ones above, then you need to index them with [Samtools](http://www.htslib.org/download/), such as: `samtools faidx hg38.fa`. When using the reference genomes linked above, you don't need to index as the index files (.fai) for these FASTA files are already provided in the [reference/genomes](reference/genomes) folder in this repository.

3. Download the latest release of ExpansionHunter and REViewer for your system:
* [https://github.com/Illumina/ExpansionHunter/releases](https://github.com/Illumina/ExpansionHunter/releases), extract files and copy the ExpansionHunter's binary file from bin/ folder into the server's [bin/](bin/) folder under the name of: ExpansionHunter.
* [https://github.com/Illumina/REViewer/releases](https://github.com/Illumina/REViewer/releases), extract file and copy the extracted file into the server's [bin/](bin/) folder under the name of: REViewer.

To allow executable access to these files, open the terminal, go to the [server's root folder](./) and run: `chmod +x genotype.sh bin/ExpansionHunter bin/REViewer`

4. Download and install [Samtools](http://www.htslib.org/download/) into your system

5. To install required Python modules for the server, run the following command in the server's root folder: `python3 -m pip install -r requirements.txt`

6. Finally, start the server: `python3 start-server.py`

7. Server should now be ready to accept your requests via STRipy's Client and is active until you terminate it (Control + C or close the terminal window).

## Configuring the server
You can configure few parameters in the server by editing the [config.json](./config.json) file. If you want to keep ExpansionHunter results or intermediate files with the variant catalogue then feel free to edit the first two or three rows.
| Parameter                | Default value                                  | Description                                                                                                    |
|--------------------------|------------------------------------------------|----------------------------------------------------------------------------------------------------------------|
| delete_results_files     | true                                           | Delete ExpansionHunter and REViewer output files? true or false                                                |
| delete_interm_files      | true                                           | Delete intermediate files (STRipy's generated BAM and the variant catalogue for ExpansionHunter) true or false |
| extensions2delete        | ".json", ".vcf", ".bam", ".bai", ".fq", ".svg", ".tsv" | List of file extensions which will be deleted when the above is set to true                                    |
| server_port              | 5000                                           | Port number which the server will be using                                                                     |
| file_catalog             | catalog.json                                   | STRipy's catalogue file location                                                                               |
| genotype_pipeline_script | ./genotype.sh                                  | Genotyping shell script file locatio                                                                           |
| eh_results_folder        | ./results/                                     | ExpansionHunter output files path                                                                              |
| interm_files_folder      | ./results/temp/                                | Intermediate files path                                                                                        |
| supported_genomes        | "hg38", "hg19"                                 | List of supported genomes                                                                                      |


## Contact
To contact the author then please use the contact form on [this page](https://stripy.org/about).
