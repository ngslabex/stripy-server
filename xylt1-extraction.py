import os
import sys
import pysam
from collections import defaultdict

def read_pair_generator(bam, read_dict):
	for read in bam.fetch():
		if not read.is_paired or read.is_secondary or read.is_supplementary:
			continue

		qname = read.query_name
		if qname not in read_dict:
			if read.is_read1:
				read_dict[qname][0] = read
			elif read.is_read2:
				read_dict[qname][1] = read
		else:
			if read.is_read1:
				yield read, read_dict[qname][1]
			elif read.is_read2:
				yield read_dict[qname][0], read
			del read_dict[qname]

def bam2fast(inputfile, outputfolder):
	bamfile = pysam.AlignmentFile(inputfile, "rb")

	# Extract out reads and save as FASTQ
	fastq1_reads = []
	fastq2_reads = []

	read_dict = defaultdict(lambda: [None, None])
	for read1, read2 in read_pair_generator(bamfile, read_dict):
		if read1 and read2:
			fastq1_reads.append(">%s\n%s\n+\n%s\n" % (read1.query_name, read1.query_sequence, "".join(map(lambda x: chr( x+33 ), read1.query_qualities))))
			fastq2_reads.append(">%s\n%s\n+\n%s\n" % (read2.query_name, read2.query_sequence, "".join(map(lambda x: chr( x+33 ), read2.query_qualities))))

	# Write FASTQ file
	inputfilename = os.path.splitext(os.path.basename(inputfile))[0]

	with open(outputfolder + inputfilename + ".regions_R1.fq", "w") as fastq_out:
		for r in fastq1_reads:
			fastq_out.write(r)

	with open(outputfolder + inputfilename + ".regions_R2.fq", "w") as fastq_out:
		for r in fastq2_reads:
			fastq_out.write(r)

if __name__ == '__main__':
	inputfilepath = sys.argv[1]
	outputfolderpath = sys.argv[2]
	bam2fast(inputfilepath, outputfolderpath)
