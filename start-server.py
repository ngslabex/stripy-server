"""
    STRipy
    Copyright (C) 2021  Andreas Halman

    STRipy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    STRipy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with STRipy. If not, see <http://www.gnu.org/licenses/>.
"""

import os
import uuid
import time
import json
import regex
import pysam
import base64
import pdfCreator
import numpy as np
from subprocess import call
from pyramid.response import Response
from pyramid.config import Configurator
from wsgiref.simple_server import make_server

__author__ = "Andreas Halman"
__contact__ = "dev@stripy.org"
__url__ = "https://stripy.org"
__copyright__ = "Copyright (C) 2021, Andreas Halman"
__license__ = "GPLv3"
__version__ = "2.0"


# Load configuration
with open('config.json', 'r') as f: config = json.load(f)
config["extensions2delete"] = tuple(config["extensions2delete"])

def versiontuple(version_str):
	"""Converts software version number to tuple in order to compare versions

	Args:
		version_str (str): Software version as a string

	Returns:
		tuple: Software version as a tuple
	"""

	return tuple(map(int, (version_str.split("."))))


def getDiseaseInfo(req_disease, req_locus):
	"""Find the matching disease or locus/gene in the catalogue and return all data for the matching locus

	Args:
		req_disease (str): Abbreviation of the disease
		req_locus (str): Name of the locus

	Returns:
		dict: Variant information, or return False if nothing was found
	"""

	with open(config['file_catalog'], mode = 'r') as cat_file:
		catalog = json.load(cat_file)

		for entry in catalog:
			matching_locus = True if entry['Locus'] == req_locus else False
			matching_disease = True if [key for key, val in entry['Diseases'].items() if key == req_disease] else False

			if matching_locus or matching_disease:
				entry.update({
					'MotifLength': len(entry['MotifPlusStrand']),
				})
				return entry

		else:
			return False


def validateInput(input_text):
	"""Check whether an input text is alphanumeric (and can also include dash and colon)

	Args:
		input_text (str): Input text

	Returns:
		str: Return the input text or empty string if it is not alphanumeric
	"""

	return input_text if regex.match(r'^\w+[-]?[:]?\w+$', input_text) else '' 


def alleleSplitToNumArray(string, split_char):
	"""Split a string of genotypes to integer list

	Args:
		string (str): Alleles' genotype as a string (e.g. 10/20)
		split_char (str): Character separating genotypes (e.g. /)

	Returns:
		list: Genotypes as a list
	"""

	return list(map(int, string.split(split_char)))


def binaryToBase64(file_content):
	"""Convert content of a binary file to base64

	Args:
		file_content (str): Binary file content

	Returns:
		str: Binary file content in base64 encoding
	"""

	pdf_file_base64 = base64.b64encode(file_content)
	pdf_base64_message = pdf_file_base64.decode('utf-8')

	return pdf_base64_message


def deleteFiles(folder, file_uuid, extensions):
	"""Delete a file with specific extension

	Args:
		folder (str): Folder where files will be searched and deletion when the extension matches
		file_uuid (str): Anonymised unique file name of the sample
		extensions (tuple): File extensions which will be deleted

	Returns:
		bool: Returns True if files were succesfully deleted
	"""

	filelist = os.listdir(folder)
	for file in filelist:
		if file_uuid in file and file.endswith(extensions):
			try:
				os.remove(os.path.join(folder, file))
			finally:
				print("File deleted: " + os.path.join(folder, file))

	return True


def deleteSampleFiles(randomfilename):
	"""Iniate deletion of files of a given sample

	Args:
		randomfilename (str): Anonymised unique file name of the sample

	Returns:
		bool: Returns True if at least some files were deleted or False if nothing was deleted
	"""

	del_temp = deleteFiles(config['interm_files_folder'], str(randomfilename), config['extensions2delete']) if config['delete_interm_files'] == True else False
	del_results = deleteFiles(config['eh_results_folder'], str(randomfilename), config['extensions2delete']) if config['delete_results_files'] == True else False

	return True if del_temp or del_results else False

def getReferenceFastaSequence(fastafile, region, flanking_size = 0):
	"""Get reference fasta sequence for given bases

	Args:
		fastafile (str): Fasta file path
		region (str): Region where sequence will be extracted out

	Returns:
		list: Sequence bases
	"""
	chrom, start, end = regex.split(':|-', region)

	start = int(start) - flanking_size 
	end = int(end) + flanking_size

	fasta_open = pysam.Fastafile(fastafile)
	seq_fasta = fasta_open.fetch(chrom, start, end)

	refseq = []
	refseq = [str(e).upper() for e in seq_fasta]

	return refseq


def determineDiagnose(repeat_length, normal_range, intermediate_range, path_range_min):
	"""Based on the range of repeats specified in the catalogue, decide whether the repeat is normal, pathogenic, intermediate or unknown

	Args:
		repeat_length (int): Number of repeats in the allele
		normal_range (dict): Maximum value of the normal range
		intermediate_range (dict): Minimum and maximum value of the intermediate range
		path_range_min (int): Pathogenic cut-off value

	Returns:
		str: Returns whether the repeat length belongs into normal, intermediate, pathogenic or unknown range
	"""
	
	if normal_range == "NA" and intermediate_range == "NA" and path_range_min == "NA": # If we have custom loci and everything is NA then it's unknown range
		return "unknown"
	else:
		if repeat_length >= normal_range['Min'] and repeat_length <= normal_range['Max']:
			diagnose = "normal"
		elif repeat_length >= path_range_min:
			diagnose = "pathogenic"
		else:
			if intermediate_range != "NA":
				if repeat_length >= intermediate_range['Min'] and repeat_length <= intermediate_range['Max']:
					diagnose = "intermediate"
				else:
					diagnose = "unknown"
			else:
				diagnose = "unknown"

	return diagnose

def determinePopulationOutlier(allele_len, pop_repeats, pop_counts):
	"""Determines whether allele length is a population outlier

	Args:
		allele_len (int): Allele length in repeats
		pop_repeats (array): Population repeats
		pop_counts (array): Counts for population repeats

	Returns:
		bool: True if is a population outlier, otherwise False
	"""
	zscore_threshold = 3.718 # 1 in 1000

	pop_repeats = [int(e) if e.isdigit() else e for e in pop_repeats.split(', ')]
	pop_counts = [int(e) if e.isdigit() else e for e in pop_counts.split(', ')]

	data = []
	for index, rep in enumerate(pop_repeats):
		for i in range(0, pop_counts[index]):
			data.append(rep)

	mean = np.mean(data)
	std = np.std(data)
	zscore = round(abs((allele_len-mean)/std), 3) if std > 0 else False

	is_outlier = True if abs(zscore) >= zscore_threshold else False

	return is_outlier, zscore


def replaceSVGColours(content, normal_range, intermediate_range, path_range_min):
	"""Replace colours in the REViewer's output SVG file based on the range where the repeat falls into

	Args:
		content (str): SVG file content
		normal_range (dict): Maximum value of the normal range
		intermediate_range (dict): Minimum and maximum value of the intermediate range
		path_range_min (int): Pathogenic cut-off value

	Returns:
		str: SVG file content after colourisation
	"""
	
	repeat_units = int(regex.search(r'(\d*) units', content).group(1))

	if determineDiagnose(repeat_units, normal_range, intermediate_range, path_range_min) == "normal":
		section_new = regex.sub('#fc8d62', '#71bf2c', content)
		section_new = regex.sub('#OrangeWhiteOrange', '#GreenWhiteGreen', section_new)

	elif determineDiagnose(repeat_units, normal_range, intermediate_range, path_range_min) == "pathogenic":
		section_new = regex.sub('#fc8d62', '#d42859', content)
		section_new = regex.sub('#OrangeWhiteOrange', '#RedWhiteRed', section_new)

	elif determineDiagnose(repeat_units, normal_range, intermediate_range, path_range_min) == "unknown":
		section_new = regex.sub('#fc8d62', '#9c9c9c', content)
		section_new = regex.sub('#OrangeWhiteOrange', '#GrayWhiteGray', section_new)

	elif determineDiagnose(repeat_units, normal_range, intermediate_range, path_range_min) == "intermediate":
		section_new = content

	else:
		section_new = regex.sub('#fc8d62', '#b8b8b8', content)
		section_new = regex.sub('#OrangeWhiteOrange', '#GrayWhiteGray', section_new)

	return section_new


def replaceSVGgradients(content):
	"""Replacing gradients with solid colours in SVG file. This is needed due to the lack of gradients support in SVGlib

	Args:
		content (str): SVG file content

	Returns:
		str: SVG file content after replacing gradients with solid colours
	"""

	replaced = regex.sub('url\(#GreenWhiteGreen\)', '#d3f2c7', content)
	replaced = regex.sub('url\(#OrangeWhiteOrange\)', '#ffe3bd', replaced)
	replaced = regex.sub('url\(#GrayWhiteGray\)', '#e6e6e6', replaced)
	replaced = regex.sub('url\(#RedWhiteRed\)', '#ffdede', replaced)
	replaced = regex.sub('url\(#BlueWhiteBlue\)', '#deeefa', replaced)

	return replaced

def getSVGContent(allele1_results, allele2_results, file_svg, normal_range, intermediate_range, path_range_min):
	"""Get the SVG's content and modify this so the colours would correspond to the allele length (either red for pathogenic, green for normal, orange for intermediate and grey for unknown range)

	Args:
		allele1_results (array): Allele 1 length and CI
		allele2_results (array): Allele 2 length and CI
		file_svg (str): SVG file path
		normal_range (dict): Minimum and maximum value of the normal range
		intermediate_range (dict): Minimum and maximum value of the intermediate range
		path_range_min (int): Pathogenic cut-off value

	Returns:
		str: Final SVG file content after colourisation
	"""
	
	replace_svg_to_code = '<svg id="svg" '
	replace_defs_to_code = """<defs>
			<linearGradient id="GrayWhiteGray" x1="0%" y1="0%" x2="0%" y2="100%">
			<stop offset="0%" style="stop-color:#9c9c9c;stop-opacity:0.8" />
			<stop offset="50%" style="stop-color:#9c9c9c;stop-opacity:0.1" />
			<stop offset="100%" style="stop-color:#9c9c9c;stop-opacity:0.8" />
			</linearGradient>
			<linearGradient id="RedWhiteRed" x1="0%" y1="0%" x2="0%" y2="100%">
			<stop offset="0%" style="stop-color:#ed809f;stop-opacity:0.8" />
			<stop offset="50%" style="stop-color:#ed809f;stop-opacity:0.1" />
			<stop offset="100%" style="stop-color:#ed809f;stop-opacity:0.8" />
			</linearGradient>"""

	content = open(file_svg, 'r').read()
	output_html = ""
	matches = regex.findall(r'(.*?marker-end=\"url\(#arrow\)\" />)(.*)(marker-start.*?</svg>)', content, regex.DOTALL)

	if matches:
		tmp_rgx = matches[0][0].replace('<svg ', replace_svg_to_code)
		output_html += tmp_rgx.replace("<defs>", replace_defs_to_code)
		replaceA1colours = replaceSVGColours(matches[0][1], normal_range, intermediate_range, path_range_min)
		replaceA2colours = replaceSVGColours(matches[0][2], normal_range, intermediate_range, path_range_min)
		replaceA1units = regex.sub(r'(' + str(allele1_results[0]) + ' units)', r'\1 (CI ' + allele1_results[1] + ')', replaceA1colours)
		replaceA2units = regex.sub(r'(' + str(allele2_results[0]) + ' units)', r'\1 (CI ' + allele2_results[1] + ')', replaceA2colours)
		output_html += replaceA1units
		output_html += replaceA2units

	else:
		matches = regex.findall(r'(.*?marker-end=\"url\(#arrow\)\" />)(.*)(</svg>)', content, regex.DOTALL)
		tmp_rgx = matches[0][0].replace('<svg ', replace_svg_to_code)
		output_html += tmp_rgx.replace("<defs>", replace_defs_to_code)
		replaceA1colours = replaceSVGColours(matches[0][1], normal_range, intermediate_range, path_range_min)
		output_html += regex.sub(r'(' + str(allele1_results[0]) + ' units)', r'\1 (CI ' + allele1_results[1] + ')', replaceA1colours)
		output_html += matches[0][2]

	return output_html


def extractDataFromJSON(file_json, locus_name):
	"""Open the ExpansionHunter's JSON results file and get the content

	Args:
		file_json (str): ExpansionHunter's JSON results file path
		locus_name (str): Locus name

	Returns:
		dict: LocusResults section in the JSON file which contains the genotyping information
	"""

	with open(file_json, 'r') as f:
		content = json.load(f)
		call_results = content['LocusResults'][locus_name]

	return call_results


def extractDataFromVCF(file_vcf, locus_name):
	"""Open the ExpansionHunter's VCF results file and get the total number of reads consistent with the genotype

	Args:
		file_vcf (str): ExpansionHunter's VCF results file path
		locus_name (str): Locus name

	Returns:
		dict: Number of different types of reads that are in consistent with the estimated genotype
	"""

	call_results = {}

	with open(file_vcf, 'r') as f:
		lines = f.readlines()
		for line in lines:
			if line.startswith("#"):
				continue

			filter_status = line.split("\t")[6]
			locus = regex.search(r'VARID=(.*?);REPID', line).group(1)

			if locus != locus_name:
				continue

			total_reads_spanning = line.split("\t")[9].split(":")[4]
			total_reads_flanking = line.split("\t")[9].split(":")[5]
			total_reads_inrepeat = line.split("\t")[9].split(":")[6]

			call_results.update({
				"TotalOfSpanningReads": total_reads_spanning,
				"TotalOfFlankingReads": total_reads_flanking,
				"TotalOfInrepeatReads": total_reads_inrepeat,
				"FilterStatus": filter_status,
			})

	return call_results


def genotypeSample(file_path, file_uuid, locus_name, sex, genome, variant_catalogue, req_genome_has_chr, analysis_type, region_extension_length):
	"""Run ExpansionHunter on the analysis ready BAM file received

	Args:
		file_path (str): File path of the analysis ready BAM file
		file_uuid (str): Unique file name of the sample
		sex (str): Sex of the sample
		locus_name (str): Name of the targeted locus
		genome (str): Genome assembly
		variant_catalogue (str): Personalised variant catalogue JSON file path
		req_genome_has_chr (bool): Does the genome used has 'chr' in chromosome names? True if yes, otherwise False

	Returns:
		dict: Results extracted out from ExpansionHunter output JSON and VCF files, in case of an error return error message
	"""

	call(config['genotype_pipeline_script'] + ' ' + locus_name + ' ' + file_path + ' ' + file_uuid + ' ' + genome + ' ' + sex + ' ' + variant_catalogue + ' ' + str(req_genome_has_chr) + ' ' + str(analysis_type) + ' ' + str(region_extension_length), shell = True)

	# ExpansionHunter's output file paths
	eh_results_path_uuid = os.path.join(config['eh_results_folder'], file_uuid)
	eh_results_path_json = eh_results_path_uuid + '.json'
	eh_results_path_vcf = eh_results_path_uuid + '.vcf'
	reviewer_results_path_svg = eh_results_path_uuid + '.' + locus_name + '.svg'

	# Check for results file every second and terminate the process when over 20 seconds has elapsed
	waiting_time = 20 # In seconds
	elapsed_time = 0
	
	while not os.path.exists(eh_results_path_json) and not os.path.exists(reviewer_results_path_svg) and elapsed_time <= waiting_time:
		time.sleep(1)
		elapsed_time += 1

	# If the ExpansionHunter's output files exists then extract out the results and return to the user
	if os.path.isfile(eh_results_path_json) and os.path.isfile(eh_results_path_vcf):
		extracted_results = extractDataFromJSON(eh_results_path_json, locus_name) # Extract data from json file
		extracted_total_reads = extractDataFromVCF(eh_results_path_vcf, locus_name) # Extract data from VCF file

		# Merge results from JSON and VCF file (total number of reads comes from VCF file)
		extracted_results['Variants'][locus_name].update(extracted_total_reads)
		# Determine whether analysing the locus ended up any results by looking at the count of returned reads, if zero then return an error message
		if extracted_results['Variants'][locus_name]["CountsOfFlankingReads"] == '()' and extracted_results['Variants'][locus_name]["CountsOfInrepeatReads"] == '()' and extracted_results['Variants'][locus_name]["CountsOfSpanningReads"] == '()':
			return {"Error": True, "ErrorMessage": "Genotype could not be determined, likely due to very low coverage of the targeted locus."}
		else:
			return extracted_results
	else:
		return {"Error": True, "ErrorMessage": "Error in getting results."}


def pingServer(request):
	"""Responds to a ping request

	Returns:
		json: running: true if server is up and running
	"""

	response = {"running": True}
	return Response(json.dumps(response))


def getReferenceSequence(request):
	"""Responds to a request for receiving reference sequence

	Returns:
		json: Reference sequence
	"""
	genome = request.GET.get('genome')
	coord = request.GET.get('coordinates')
	flankinglen = int(request.GET.get('flankinglen'))

	if not genome and not coord:
		return None
	
	if bool(regex.search(r'^\w.*:\d.*-\d.*', coord)): # If coordinates are not written correctly
		chrom, start, end = regex.split(':|-', coord)
		if "chr" not in chrom and len(chrom) <= 2:
			chrom = "chr"+chrom

		start = int(start)
		end = int(end)
		coord_combined = str(chrom)+":"+str(start)+"-"+str(end)

		if (genome == "hg19" or genome == "hg38") and flankinglen <= 20 and bool(regex.search(r'^chr[1-9]$|^chr1[0-9]$|^chr2[0-2]$|^chrX$|^chrY$|^[1-9]$|^1[0-9]$|^2[0-2]$|^X$|^Y$', chrom, regex.MULTILINE)) and isinstance(start, int) and isinstance(end, int) and end-start <= 200:
			reference_sequence =  getReferenceFastaSequence('reference/genomes/'+genome+'.fa', coord_combined, flankinglen)
			reference_sequence = "".join(reference_sequence)

			response = {"Sequence": reference_sequence}

			return Response(json.dumps(response))
		else:
			return None
	else:
		return None


def analyseSample(request):
	"""Iniates the analysis process. Validating input variables, creating variant catalogue for ExpansionHunter and running the tool, extracting our results and using REViewer to get read alignments, which will be then colourised, followed by generation of PDF report

	Args:
		request (POST): Data received from the Client, including all selected values, determined number of repets in flanking reads and fully repeated mates, input sequencing file and off-target regions

	Returns:
		json: Response in JSON format for the Client
	"""

	if request.method == "GET":
		return Response("Error")

	req_disease = validateInput(request.POST.get('disease'))
	req_locus = validateInput(request.POST.get('locus'))
	custom_entry = request.POST.get('custom_entry')
	req_sex = validateInput(request.POST.get('sex'))
	req_genome = validateInput(request.POST.get('genome'))
	req_genome_has_chr = validateInput(request.POST.get('genome_chr'))
	req_analysis = validateInput(request.POST.get('analysis'))
	req_limit_region = validateInput(request.POST.get('limit_region'))
	reference_coverage = request.POST.get('region_coverage')
	repeats_mates = request.POST.get('repeats_mates')
	repeats_mates = int(repeats_mates) if repeats_mates != '' else '' # Convert string to integer if a numeric value exists
	repeats_flanking = request.POST.get('repeats_flanking')
	offtargets = request.POST.get('offtargets')
	fileslist = request.POST.getall('file')

	if req_disease and req_locus == "Custom":
		req_disease = ''

	# Check for input errors
	error_message = ''
	if not req_disease and not req_locus and not req_genome and not req_sex:
		error_message = "Empty input"

	# Get information of the disease/gene that the user wants to analyse
	disease_info = getDiseaseInfo(req_disease, req_locus) if req_locus != "Custom" else json.loads(custom_entry)

	if disease_info == False:
		error_message = "Disease not supported"

	if req_genome not in config['supported_genomes']:
		error_message = "Genome not supported"

	if req_sex not in ["male", "female"]:
		error_message = "Unknown sex specified"

	if custom_entry:
		custom_coord = json.loads(custom_entry)["LocationCoordinates"][req_genome]
		custom_chrom, custom_start, custom_end = regex.split(':|-', custom_coord)

		if int(custom_end)-int(custom_start) < 1 or int(custom_end)-int(custom_start) > 200 or bool(regex.search(r'^chr[1-9]$|^chr1[0-9]$|^chr2[0-2]$|^chrX$|^chrY$|^[1-9]$|^1[0-9]$|^2[0-2]$|^X$|^Y$', custom_chrom, regex.MULTILINE)) == False:
			error_message = "Wrong coordinates"

	# In case of an error message defined, return it to user and do not continue with analysis
	if error_message:
		return Response(json.dumps({"Error": True, "ErrorMessage": error_message}))

	# Set variables for easier use
	locus_name = disease_info['Locus']

	# Anonymise data and generate random identifier for the file that was sent to server before temporary storing on disk
	randomfilename = uuid.uuid4()

	for file in fileslist:
		filetype = file.filename.split('.')[-1]

		if filetype == 'bam':
			file_path = os.path.join('%s.bam' % randomfilename)

		elif filetype == 'bai':
			file_path = os.path.join('%s.bam.bai' % randomfilename)

		with open(config['interm_files_folder'] + file_path, 'wb') as open_file:
			open_file.write(file.file.read())

	# Create a personalised variant catalogue for the sample
	ot_file_path = os.path.join('%s.json' % randomfilename)
	disease_coord = disease_info['LocationCoordinates'][req_genome]
	if not req_genome_has_chr:
		disease_coord.replace("chr", "")

	variant = {
		"LocusId": disease_info["Locus"],
		"LocusStructure": "(" + disease_info["MotifPlusStrand"] + ")*",
		"ReferenceRegion": disease_coord,
		"VariantType": "RareRepeat" if offtargets else "Repeat"
	}

	# Append detected off-target regions (where reads are actually aligned in that sample) to the catalogue when off-target regions were received (i.e. analysis type is extended)
	if offtargets:
		variant.update({
			"OfftargetRegions": json.loads(offtargets)
		})

	# Save the variant catalogue's file temporary on disk
	with open(config['interm_files_folder'] + ot_file_path, 'w') as outfile:
		json.dump(variant, outfile, indent = 4)

	# Get the genotyping results from ExpansionHunter's file
	eh_callinfo = genotypeSample(config['interm_files_folder']+str(randomfilename)+'.bam', str(randomfilename), locus_name, req_sex, req_genome, config['interm_files_folder'] + str(randomfilename)+'.json', req_genome_has_chr, req_analysis, req_limit_region)

	# Check whether there were any errors and if yes then do not proceed and delete files
	if "Error" in eh_callinfo:
		deleteSampleFiles(randomfilename) # Delete files
		return Response(json.dumps(eh_callinfo))

	# Disease of analysis is either the disease corresponding to the selected locus or the first disease in the diseases list under the selected locus
	analysed_disease = req_disease if req_disease else next(iter(disease_info['Diseases'].values()))['DiseaseSymbol']

	# Otherwise put results together before sending them back to the client
	normal_range = disease_info['Diseases'][analysed_disease]['NormalRange']
	
	if disease_info['Locus'] != 'Custom':
		if disease_info['Locus'] == 'FMR1': # Quick FMR1 fix
			path_range_min = int(disease_info['Diseases'][analysed_disease]['PathogenicCutoff'].split("-")[0])
		else:
			path_range_min = int(disease_info['Diseases'][analysed_disease]['PathogenicCutoff'])
	else:
		path_range_min = "NA"

	intermediate_range = disease_info['Diseases'][analysed_disease]['IntermediateRange']

	if '/' in eh_callinfo['Variants'][locus_name]['Genotype']:
		no_alleles = 2
		allele1_len, allele2_len = alleleSplitToNumArray(eh_callinfo['Variants'][locus_name]['Genotype'], '/')
		allele1_ci_range, allele2_ci_range = eh_callinfo['Variants'][locus_name]['GenotypeConfidenceInterval'].split('/')
	else:
		no_alleles = 1
		allele1_len, allele2_len = int(eh_callinfo['Variants'][locus_name]['Genotype']), 0
		allele1_ci_range, allele2_ci_range = eh_callinfo['Variants'][locus_name]['GenotypeConfidenceInterval'], "0-0"

	# Determine whether any of the alleles are population outliers
	if req_locus != "Custom":
		is_population_outlier_a1, zscore_a1 = determinePopulationOutlier(allele1_len, disease_info["PopulationData"]["All"]["Repeats"], disease_info["PopulationData"]["All"]["Counts"])

		if no_alleles == 1:
			is_population_outlier_a2, zscore_a2 = False, False
		elif no_alleles == 2:
			is_population_outlier_a2, zscore_a2 = determinePopulationOutlier(allele2_len, disease_info["PopulationData"]["All"]["Repeats"], disease_info["PopulationData"]["All"]["Counts"])
	else:
		is_population_outlier_a1, zscore_a1 = False, False
		is_population_outlier_a2, zscore_a2 = False, False

	# Add metadata
	call_fragment_length = eh_callinfo['FragmentLength']
	call_read_length = eh_callinfo['ReadLength']

	call_coverage = round(eh_callinfo['Coverage'], 1)
	if no_alleles == 1:
		total_reads_spanning = [int(eh_callinfo['Variants'][locus_name]['TotalOfSpanningReads']), 0]
		total_reads_flanking = [int(eh_callinfo['Variants'][locus_name]['TotalOfFlankingReads']), 0]
		total_reads_inrepeat = [int(eh_callinfo['Variants'][locus_name]['TotalOfInrepeatReads']), 0]
	elif no_alleles == 2:
		total_reads_spanning = alleleSplitToNumArray(eh_callinfo['Variants'][locus_name]['TotalOfSpanningReads'], '/')
		total_reads_flanking = alleleSplitToNumArray(eh_callinfo['Variants'][locus_name]['TotalOfFlankingReads'], '/')
		total_reads_inrepeat = alleleSplitToNumArray(eh_callinfo['Variants'][locus_name]['TotalOfInrepeatReads'], '/')

	filter_status = eh_callinfo['Variants'][locus_name]['FilterStatus']

	svg_file_path = config['eh_results_folder'] + str(randomfilename) + '.' + locus_name + '.svg'

	if os.path.isfile(svg_file_path):
		reads_svg_content = getSVGContent([allele1_len, allele1_ci_range], [allele2_len, allele2_ci_range], svg_file_path, normal_range, intermediate_range, path_range_min)
		reads_svg_content_gradients_replaced = replaceSVGgradients(reads_svg_content) # Replace gradients to solid colours
	else:
		reads_svg_content = '<svg width="1400" height="205" xmlns="http://www.w3.org/2000/svg"><text x="140" y="21" dy="0.25em" text-anchor="middle" font-family="monospace" font-size="13px">Could not create visualisations for this locus.</text></svg>'
		reads_svg_content_gradients_replaced = reads_svg_content

	with open(svg_file_path, 'w') as svgfile: svgfile.write(reads_svg_content_gradients_replaced) # Overwrite original SVG file content

	reference_sequence =  getReferenceFastaSequence('reference/genomes/'+req_genome+'.fa', disease_info['LocationCoordinates'][req_genome], 300) # 300 bp as flanking size
	reference_coverage = [int(e) if e.isdigit() else e for e in reference_coverage.split(', ')] # Make reference coverage list to array
	ref_coverage_response = {"Bases": reference_sequence, "Values": reference_coverage }

	response = {
		"Files": {},
		"SampleResults": {
			"AnalysisType": req_analysis,
			"SampleSex": req_sex,
			"Allele1Len": allele1_len,
			"Allele2Len": allele2_len,
			"Allele1CI": allele1_ci_range,
			"Allele2CI": allele2_ci_range,
			"Allele1Zscore": zscore_a1,
			"Allele2Zscore": zscore_a2,
			"Allele1IsOutlier": is_population_outlier_a1,
			"Allele2IsOutlier": is_population_outlier_a2,
			"Coverage": call_coverage,
			"ReadLength": call_read_length,
			"FragmentLength": call_fragment_length,
			"Genome": req_genome,
			"AnalysedDisease": analysed_disease,
			"AnalysedReferenceCoordinates": disease_info['LocationCoordinates'][req_genome],
			"MatesFullOfRepeats": repeats_mates,
			"HighestPathRepeatsInFlanking": repeats_flanking,
			"TotalOfSpanningReads": total_reads_spanning,
			"TotalOfFlankingReads": total_reads_flanking,
			"TotalOfInrepeatReads": total_reads_inrepeat,
			'CoverageOnReference': ref_coverage_response,
			"FilterStatus": filter_status,
			"ReadsSVG": reads_svg_content
		}
	}
	response.update({"VariantInfo": disease_info})

	 # Remove one line from SVG and overwrite the file content before creating PDFs
	reads_svg_content_gradients_replaced = reads_svg_content_gradients_replaced.replace('stroke="black" marker-start="url(#arrow)" marker-end="url(#arrow)"', '')
	with open(svg_file_path, 'w') as svgfile: svgfile.write(reads_svg_content_gradients_replaced)

	# Create PDF report and read visualisation file
	pdf_report = binaryToBase64(pdfCreator.createPDFreport(response, svg_file_path))
	pdf_readsvis = binaryToBase64(pdfCreator.convertSVGtoPDF(svg_file_path))

	response["Files"].update({"PDFreport": pdf_report,
							  "PDFreadsvis": pdf_readsvis})

	deleteSampleFiles(randomfilename) # Delete files

	return Response(json.dumps(response))


def main():
	"""Creates the listening server
	"""

	with Configurator() as serverconf:
		serverconf.add_route('pingServer', '/ping')
		serverconf.add_route('getReferenceSequence', '/refsequence')
		serverconf.add_route('analyseSample', '/analyse')
		serverconf.add_view(pingServer, route_name = 'pingServer', renderer = 'json', request_method = ('GET'))
		serverconf.add_view(getReferenceSequence, route_name = 'getReferenceSequence', renderer = 'json', request_method = ('GET'))
		serverconf.add_view(analyseSample, route_name = 'analyseSample', renderer = 'json', request_method = 'POST')
		application = serverconf.make_wsgi_app()

	server = make_server('0.0.0.0', config["server_port"], application)

	print("""
	* STRipy | """ + __copyright__ + """
	* This program comes with ABSOLUTELY NO WARRANTY.
	* This is free software, and you are welcome to redistribute it
	* under certain conditions. Read more at """ + __url__ + """

	""")
	print(".:: STRipy's Server v" + __version__ + " has started ::.")
	server.serve_forever()


if __name__ == '__main__':
	main()